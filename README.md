# 前端 JS 工具库

#### 介绍
1. 总结前端常用js方法
2. 每个方法可以单独引入（性能优越）

#### 软件架构

```md
.
|—— demo                      // 测试例子
|—— dist                      // 打包生成文件
|—— src                       // 源码文件
|   |—— cookie.js             // cookie方法
|   |—— delayOperation.js     // 延时方法
|   |—— env.js                // 判断设备环境
|   |—— jq.js                 // 原生分装jq  
|   |—— mathOperation.js      // 运算方法
|   |—— rem.js                // 布局方法
|   |—— times.js              // 时间方法
|   |—— url.js                // url链接方法
|   |—— validation.js         // 表单验证
|   |—— filter.js             // 过滤器
|   |—— array.js              // 操作数组
|   └── index.js              // 入口文件
|—— test                      // 测试文件
|—— .babelrc
|—— .eslintrc.js
|—— .gitignore                // 限制git上传文件
|—— .babelrc
|—— package.json              // 本地npm管理文件  
|—— JEST.md                   // 单元测试说明
|—— CHANGELOG.md              // 更新日志
|—— README.md                 // 项目说明
|—— webpack.config.js         // 打包脚本
.

## 备注：
#### 1.为什么原生分装jq方法     原因：jq库过于庞大，我们只封装常用的

```
#### 安装教程

1. 通过npm安装 npm install hjm-web-utils -d

1. 直接git clone https://gitee.com/hjm100/hjm-web-utils.git

1. 使用 npm run build 生成dist文件，直接饮用dist/main.js即可

#### 使用说明

1. 通过 npm 安装 hjm-web-utils 模块

1. 在项目中使用

```js

//引入需要用到的方法
import { htmlSetFontSize, isIos } from 'hjm-web-utils'

// 直接使用方法
htmlSetFontSize(720)
console.log(isIos())

// 支持所有环境引入使用

```

#### API(具体使用方法查看源文件src/)

##### cookieAPI

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| setCookie | (name, value) | 设置cookie |
| getCookie | name | 获取cookie |
| clearCookie | name | 清除cookie |

##### arrayAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| sortData | (data, key, direction:asc升 desc降) | 根据key值进行数组排序 |

##### delayOperationAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| throttle | (fn, time) | 延时处理方法 |
| debounce | (fn, time) | 延迟处理方法 |

##### envAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| isIos | 无 | 判断是否是iOS设备 |
| isAndroid | 无 | 延迟处理方法 |
| isAlipay | 无 | 判断是否是支付宝 |
| isWeChat | 无 | 判断是否是微信环境 |
| isQQ | 无 | 判断是否是QQ环境 |
| isYiXin | 无 | 判断是否是易信 |
| isSupportWebp | 无 | 判断是否支持webp |
| judgeScreen | 无 | 判断横竖屏（0横屏1竖屏）|

##### jqAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| $ | selector | 模拟jq风格选择器 |

##### mathOperationAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| add | (x, y) | 加法运算 |
| mul | (x, y) | 乘法操作 |
| concat | (x, y) | 数组拼接操作 |
| asyncAdd | (x, y) | 异步加法 |
| mul | (x, y) | 乘法操作 |

##### filterAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| trimNum | (num, divisor:需要转化的单位, units转化为的单位, digits:小数位数) | 对数字进行处理，以符合要求规范 |

##### remAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| htmlSetFontSize | resultWidth:设计稿宽 | 设置rem布局(设计稿宽默认720) |
| getCurrentPx | (rem,resultWidth:设计稿宽) | 根据传入的rem值计算当前界面中应显示的px值|

##### timesAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| timeFormate | (timestamp:时间戳, style:格式) | 时间戳格式化工具(格式：YY-MM-DD HH:mm:ss)|
| secondsToTime | seconds:秒 | 秒转化为时分秒|

##### urlAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| getUrlString | name(参数key值) | 截取url传递的参数 |
| httpToHttps | URL(链接地址) | http链接转为https|

##### validationAPI:

| 方法 | 参数 | 备注 |
| :-: | :-: | :-: |
| isIDCard | idcard(身份证号) | 校验身份证号 |
| isNickName | name(字符串) | 用户名验证 (字母数字汉字 长度 3-->8) |
| isMobile | mobile(手机号) | 手机号验证(精确验证需要关注运营商更新号段信息) |
| isPassword | password(字符串) | 密码验证  (包含数字字母 长度为6-->16) |

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
