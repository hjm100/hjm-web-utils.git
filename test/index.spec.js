/**
 * 引入测试的函数
 */
const getUrlString = require('../src/index')

describe("getUrlString", () => {
  it("测试是否可以获取到url参数", () => {
    let url = "https://baidu.com?name=baidu";
    assert(getUrlString("name") === "baidu");
  });
});
