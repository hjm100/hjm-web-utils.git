const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  entry: path.resolve(__dirname, './index.js'),
  output: {
    path: path.resolve(__dirname, './dist')
  },
  mode: 'production', // 自动集成打包
  module: {
    rules: [{
      test: /\.js$/,
      use: ['babel-loader'],
      include: path.join(__dirname, './'),
      exclude: /node_modules/,
      sideEffects: false // 让 webpack 去除 tree shaking 带来副作用的代码。
    }]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: './index.html',
      template: path.resolve(__dirname, './index.html'),
      inject: true,
      minify: { // 压缩html
        removeComments: true, // 删除注释
        collapseWhitespace: true, // 删除空格
        removeAttributeQuotes: true, // 是去掉属性的双引号
        minifyCSS: true // 删除样式中的空格
      }
    })
  ]
}
