/**
 * 对数字进行处理，以符合要求规范
 * @param {需要转化的数字} num
 * @param {需要转化的单位} divisor
 * @param {转化为的单位} units
 * @param {小数位数} digits
 */
export let trimNum = (num, divisor, units, digits) => {
  if (num > divisor) {
    return num / divisor.toFixed(digits) + units
  }
  return num
}
