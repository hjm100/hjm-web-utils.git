/**
 * 判断是否是iOS设备
 */
export let isIos = () => {
  const ua = navigator.userAgent.toLowerCase()
  if (/(iPhone|iPad|iPod|iOS)/i.test(ua)) {
    return true
  }
  return false
}

/**
 * 判断是否是支付宝
 */
export let isAlipay = () => {
  const userAgent = navigator.userAgent.toLowerCase()
  if (userAgent.match(/Alipay/i) === 'alipay') {
    return true
  } else {
    return false
  }
}

/**
 * 判断是否是易信
 */
export let isYiXin = () => {
  const ua = navigator.userAgent.toLowerCase()
  if (/YiXin/i.test(ua)) {
    return true
  }
  return false
}

/**
 * 判断是否是微信环境
 */
export let isWeChat = function() {
  const ua = navigator.userAgent.toLowerCase()
  if (/MicroMessenger/i.test(ua)) {
    return true
  } else {
    return false
  }
}

/**
 * 判断是否是QQ环境
 */
export let isQQ = function() {
  const ua = navigator.userAgent.toLowerCase()
  if (ua.match(/\sQQ/i) !== null) {
    return true
  } else {
    return false
  }
}

/**
 * 判断是否是Android环境
 */
export let isAndroid = function() {
  let ua = navigator.userAgent.toLowerCase()
  if (/Android/i.test(ua)) {
    return true
  }
  return false
}

/**
 * 判断是否支持webp
 */
export let isSupportWebp = () => {
  let isSupportWebp =
    !![].map &&
    document
      .createElement('canvas')
      .toDataURL('image/webp')
      .indexOf('data:image/webp') === 0
  return isSupportWebp
}

/**
 * 判断横竖屏
 * 横屏 return 0
 * 竖屏 return 1
 */

export let judgeScreen = () => {
  let width = document.body.clientWidth
  let height = document.body.clientHeight
  if (width > height) {
    return true
  }
  return false
}
