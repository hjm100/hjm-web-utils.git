/**
 * 延时处理方法
 * @param {func} fn 延时处理的方法
 * @param {float} time 延时时间
 */
const throttle = (fn, time) => {
  let timer
  return function(...args) {
    if (!timer) {
      timer = setTimeout(() => {
        return (timer = null)
      }, time)
      return fn.apply(this, args)
    }
  }
}

/**
 * 延迟处理方法
 * @param {func} fn 处理的方法
 * @param {float} time 延迟时间
 */
const debounce = (fn, time) => {
  let timer
  return function(...args) {
    clearTimeout(timer)
    timer = setTimeout(() => {
      return fn.apply(this, args)
    }, time)
  }
}

export { throttle, debounce }
