/**
 * 校验身份证号
 * @param {身份证号} idcard
 */
export let isIDCard = idcard => {
  idcard = idcard.toUpperCase() // 对身份证号码做处理
  var ereg
  var Y, JYM
  var S, M
  var idCardArrary = []
  idCardArrary = idcard.split('')
  /* 身份号码位数及格式检验 */
  // 18位身份号码检测
  // 出生日期的合法性检查
  // 闰年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))
  // 平年月日:((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))
  if (
    parseInt(idcard.substr(6, 4)) % 4 === 0 ||
    (parseInt(idcard.substr(6, 4)) % 100 === 0 &&
      parseInt(idcard.substr(6, 4)) % 4 === 0)
  ) {
    ereg = /^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}[0-9XxAa]$/ // 闰年出生日期的合法性正则表达式
  } else {
    ereg = /^[1-9][0-9]{5}19[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|3[0-1])|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|1[0-9]|2[0-8]))[0-9]{3}[0-9XxAa]$/ // 平年出生日期的合法性正则表达式
  }
  if (ereg.test(idcard)) {
    // 测试出生日期的合法性
    // 计算校验位
    S =
      (parseInt(idCardArrary[0]) + parseInt(idCardArrary[10])) * 7 +
      (parseInt(idCardArrary[1]) + parseInt(idCardArrary[11])) * 9 +
      (parseInt(idCardArrary[2]) + parseInt(idCardArrary[12])) * 10 +
      (parseInt(idCardArrary[3]) + parseInt(idCardArrary[13])) * 5 +
      (parseInt(idCardArrary[4]) + parseInt(idCardArrary[14])) * 8 +
      (parseInt(idCardArrary[5]) + parseInt(idCardArrary[15])) * 4 +
      (parseInt(idCardArrary[6]) + parseInt(idCardArrary[16])) * 2 +
      parseInt(idCardArrary[7]) * 1 +
      parseInt(idCardArrary[8]) * 6 +
      parseInt(idCardArrary[9]) * 3
    Y = S % 11
    M = 'F'
    JYM = '10X98765432'
    M = JYM.substr(Y, 1)
    /* 判断校验位 */
    if (M === idCardArrary[17]) {
      /* 检测ID的校验位false; */
      return true
    } else if (idCardArrary[17] === 'A') {
      // A结尾不校验规则
      return true
      /* 检测ID的校验位false; */
    } else {
      return false
    }
  } else {
    return false
  }
}

/**
 * 用户名验证 (字母数字汉字 长度 3-->8)
 * @param {name} 字符串
 */
export let isNickName = name => {
  let jubName = /^[\w\u4e00-\u9fa5]{1,8}$/
  if (jubName.test(name)) return true
  else return false
}

/**
 * 手机号验证(精确验证需要关注运营商更新号段信息)
 * @param {mobile} 手机号
 */
export let isMobile = mobile => {
  let jubMobile = /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[45678]|19[89]|16[6])[0-9]{8}$/
  if (jubMobile.test(mobile)) return true
  else return false
}

/**
 * 密码验证  (包含数字字母 长度为6-->16)
 * @param  {password} 字符串
 */
export let isPassword = password => {
  let jubPas = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/
  if (jubPas.test(password)) return true
  else return false
}
