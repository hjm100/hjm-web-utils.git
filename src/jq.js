/**
 * 模拟jq风格选择器
 * @param {selector} selector 选择器
 */
export const $ = selector => {
  return document.querySelector(selector)
}
