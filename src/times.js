/**
 * 时间戳格式化工具
 * @param {时间戳} timestamp
 * @param {格式} style
 */
export let timeFormate = (timestamp, style) => {
  // 首先判定sytle的格式
  let date = new Date(timestamp)
  let year = date.getFullYear()
  let month = (date.getMonth() + 1).toString().padStart(2, 0)
  let day = date
    .getDate()
    .toString()
    .padStart(2, 0)
  let hour = date
    .getHours()
    .toString()
    .padStart(2, 0)
  let minute = date
    .getMinutes()
    .toString()
    .padStart(2, 0)
  let seconds = date
    .getSeconds()
    .toString()
    .padStart(2, 0)
  return style
    .replace('YY', year)
    .replace('MM', month)
    .replace('DD', day)
    .replace('HH', hour)
    .replace('mm', minute)
    .replace('ss', seconds)
}

/**
 * 秒转化为时分秒
 */
export let secondsToTime = seconds => {
  let d = Math.floor(seconds / 86400).toString()
  let h = Math.floor((seconds % 86400) / 3600)
    .toString()
    .padStart(2, 0)
  let m = Math.floor((seconds % 3600) / 60)
    .toString()
    .padStart(2, 0)
  let s = ((seconds % 3600) % 60).toString().padStart(2, 0)
  let final = d !== '0' ? `${d}天${h}:${m}:${s}` : `${h}:${m}:${s}`
  return final
}
