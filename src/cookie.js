/**
 * 设置cookie
 * @param name    key   值
 * @param value   value 值
 */

export let setCookie = (name, value) => {
  var expiryday = new Date()
  expiryday.setTime(expiryday.getTime() + 30 * 30 * 24 * 60 * 60 * 1 * 1000)
  document.cookie =
    name + '=' + escape(value) + '; expires=' + expiryday.toGMTString()
}

/**
 * 获取cookie
 * @param name    key值
 */

export let getCookie = name => {
  var arr = document.cookie.match(new RegExp('(^| )' + name + '=([^;]*)(;|$)'))
  if (arr != null) return unescape(arr[2])
  return null
}

/**
 * 清除cookie
 * @param name    key值
 */

export let clearCookie = name => {
  var exp = new Date()
  exp.setTime(exp.getTime() - 1)
  var cval = getCookie(name)
  if (cval != null) {
    document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString()
  }
}
