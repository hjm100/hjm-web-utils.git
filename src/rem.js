/**
 * 采用网易的做法
 * @param {resultWidth} 设计图宽
 * rem布局单位设置font-size大小
 */
const htmlSetFontSize = (resultWidth = 720) => {
  let docEl = document.documentElement
  let resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
  let recalc = () => {
    let h = window.innerHeight
    let w = window.innerWidth
    // 横屏情况下，以height为width
    let width = w > h ? h : w
    width = width > resultWidth ? resultWidth : width
    let fz = 100 * (width / resultWidth)
    docEl.style.fontSize = fz + 'px'
  }
  window.addEventListener(resizeEvt, recalc, false)
}

/**
 * 根据传入的rem值计算当前界面中应显示的px值
 *  @param {rem}
 *  @param {resultWidth} 设计图宽
 */
let getCurrentPx = (rem, resultWidth = 720) => {
  let viewPort = window.screen.width
  let rootFontSize = 100 * (viewPort / 720)
  return rootFontSize * rem
}

export { htmlSetFontSize, getCurrentPx }
