/**
 * 截取url传递的参数
 * @param name   参数key值
 */

export let getUrlString = name => {
  const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  let params = window.location.search
  if (!params) {
    params = window.location.hash
  }
  params = params.split('?')[1]
  if (!params) return null
  let r = params.match(reg)
  if (r != null) return r[2]
  return null
}

/**
 * http链接转为https
 * @param URL   链接地址
 */

export let httpToHttps = URL => {
  return URL.replace(/https|http/, 'https')
}
