/** 数组排序
 * data: array
 * key: the key you want to sort
 * direction: 'asc' or 'desc'  asc升 desc降
 */
export let sortData = (data, key, direction) => {
  return data.sort(function(a, b) {
    // 三目运算符：也可以自该为if语句
    return direction === 'desc'
      ? parseFloat(a[key]) < parseFloat(b[key])
      : parseFloat(a[key]) > parseFloat(b[key])
  })
}
